<?php

require_once ("config.php"); // logowanie do bazy mysql
require_once ("funkcje.php"); // pomocnicze funkcje

if (isset($_POST['submit']) && $_POST['submit'] == 'Zaloguj') { // jeśli wciśnięty przyciski zaloguj
    session_start();

    $error = array(); // tworzę tablicę błędów


    if (empty($_POST['email']) || empty($_POST['haslo']))
        $error[] = 'Wszystkie pola muszą być wypełnione!';
    if (!count($error)) {
        $_POST['login'] = clear($_POST['email']); //czyszcze dane od użytkownika
        $_POST['haslo'] = clear($_POST['haslo']); //

        $row = mysql_fetch_assoc(mysql_query("SELECT id,login,email,admin FROM uzytkownicy WHERE email='" . $_POST['email'] . "' AND haslo='" . szyfruj($_POST['haslo']) . "'"));
        if ($row['login']) { // szukam użytkownika o podanym emialu i haśle // jeśli znaleźony to tworzę sesje
            $_SESSION['logged'] = true;
            $_SESSION['login'] = $row['login'];
            $_SESSION['id'] = $row['id'];
            if ($row['admin'] == 1)
                $_SESSION['admin'] = true; // jeśli użytkownik to admin dodaj odpowiednią zmienną do sesjii
            $_SESSION['success'] = 'Zalogowano!';
            header("Location: index.php"); // wracam na stronę główną
            exit;
        } else
            $error[] = 'Zła nazwa użytkownika i / lub hasło!';
    }

    if ($error)
        $_SESSION['error'] = implode('<br />', $error); // sumuje błędy

    header("Location: index.php?a=logowanie"); // wracam na strone logowania
    exit;
}
?>

<div class="row">

    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-6">



                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Logowanie</h4>
                    </div>
                    <div class="panel-body">

                        <?php odpowiedzi(); ?>

                        <form role="form" action="login.php" method="post">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Adres email</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Adres email" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Hasło</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Hasło" name="haslo" required>
                            </div>
                            <button type="submit" class="btn btn-default" name="submit" value="Zaloguj">Zaloguj</button>
                        </form>

                    </div>
                </div>

            </div>
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Nie masz konta?</h4>
                    </div>
                    <div class="panel-body text-center">
                        <a href="?a=rejestracja">
                            <button type="button" class="btn btn-primary btn-lg">Załóż nowe konto</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
