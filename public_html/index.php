<?php
ini_set('display_errors', 1);           //  Wyświtlanie błędów
ini_set('display_startup_errors', 1);   //
error_reporting(-1);                    //
require_once 'config.php';
require_once 'funkcje.php'; //  wczytuje pomocnicze funkcje

session_start(); //rozpoczynam sesje (potrzebne do logowania)

if (!isset($_SESSION['logged'])) {// ! oznacza zaprzeczenie, czyli jeśli nie ma zmiennej zalogowany to wstawiam ustawienia osoby niezalogowanej
    $_SESSION['logged'] = false;
    $_SESSION['admin'] = false;
    $_SESSION['user_id'] = - 1;
}

if (isset($_GET['wyloguj'])) {// jeśli został kliknięty klawisz wyloguj usuń sesję 
    $_SESSION = array();
    session_destroy();
    header("Location: index.php"); //... i przejć na strone główną
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="OgĹoszenia">
        <meta name="author" content="ADIN">
        
        <title>Mapa Polski</title>
        <!-- Style -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin-ext' rel='stylesheet' type='text/css'>
        
<!-- Bootstrap -->
<link href="http://bootswatch.com/cerulean/bootstrap.min.css" rel="stylesheet">
<link href="http://bootswatch.com/cerulean/bootstrap.css" rel="stylesheet">
<style>
    @media (min-width: 980px) {
  body {
    padding-top: 60px;
    padding-bottom: 42px;
  }
}
</style>
        

        <!-- Wyjątki dla starych przeglądarek -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Javascript -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
        
  
        
        
    </head>

    <body role="document">
        
  
        <div role="navigation" class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="index.php" class="navbar-brand"><b>Mapa Polski</b></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="?a=dodaj"><b>Dodaj zdarzenie</b></a></li>
          </ul>
 

   <?php
                    if (!$_SESSION['logged']) {//jeśli niezalogowany, pokaż linki do logowania/rejestracji
                        ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="?a=rejestracja">Załóż konto</a>
                            </li>
                            <li><a href="?a=logowanie">Zaloguj</a>
                            </li>
                        </ul>
                        <?php
                    } else { // jeśli zalogowany, pokaż nazwę konta i przycisk wyloguj
                        ?>
                        <ul class="nav navbar-nav navbar-right">

                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" role="button" id="drop4">
                                    Konto <?php echo $_SESSION['login']; ?> 
                                    <b class="caret"></b></a>
                                <ul aria-labelledby="drop4" role="menu" class="dropdown-menu">
    <?php if ($_SESSION['admin']) { ?> <li role="presentation"><a href="?a=admin" tabindex="-1" role="menuitem">Panel Administratora</a><?php } ?>
                                    </li>
                                </ul>
                            </li>

                            <li><a href="?wyloguj">Wyloguj</a>
                            </li>

                        </ul>
<?php } // koniec instrukcji warunkowej 'czy zalogowany'  ?>               
              

        </div><!--/.nav-collapse -->
      </div>
    </div>
       
        
  
        
        
        <div class="container" role="main" >
            <div class="row">
 
                <div class="col-md-1"></div>
                <div class="col-md-10">
<?php
if (isset($_SESSION['error']) && $_SESSION['error']) {
    echo '<div class="alert alert-danger">' . $_SESSION['error'] . '</div>';
    unset($_SESSION['error']);
}

if (isset($_SESSION['success']) && $_SESSION['success']) {
    echo '<div class="alert alert-success">' . $_SESSION['success'] . '</div>';
    unset($_SESSION['success']);
}

if (isset($_GET['a'])) { //jeśli istnieje zmienna a, a od action=akcja
    $a = clear($_GET['a']); //czyszczę zmienną z potencjalnie niebezpiecznego kodu
    switch ($a) { //wczytuje strone w zależności od wartośći zmiennej $a
        case 'logowanie':
            include ('login.php');

            break;

        case 'rejestracja':
            include ('register.php');

            break;

        case 'dodaj':
            include ('dodaj.php');

            break;
        
        case 'admin':
            include ('admin.php');

            break;


        default:
            include ('home.php');

            break;
    }
}
else { // jeśli zmienna $a nie istnieje wczytaj strone główną
    include ('home.php');
}
?>
                </div>
<div class="col-md-1"></div>
            </div>
        </div>
    </body>

</html>