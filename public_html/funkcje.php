<?php

// lista funkcji pomocniczych

function checkEmail($email) { //sprawdzam poprawność adresu email
    if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9._-])*@([a-zA-Z0-9_-])+
([a-zA-Z0-9._-]+)+$/", $email)) {
        return false;
    } else {
        return true;
    }
}

function clear($text) { // czyszczę dane wysyłane przez użytkownika
    $text = trim($text); // usuwam białe znaki na początku i na końcu
    $text = mysql_real_escape_string($text); // filtrujem tekst aby zabezpieczyć się przed sql injection
    $text = htmlspecialchars($text); // dezaktywujemy kod html
    return $text;
}

function szyfruj($password) {
    return sha1(md5($password)); // szyfruję hasło (wrazie dostępu osób trzecich do bazy nie będzie możliwe odczytanie haseł użytkowników)
}

function odpowiedzi() { // wyświetlanie odpowiedzi z formularzy
    if (isset($_SESSION['error']) && $_SESSION['error']) {// wyświetlanie błędów jeśli istnieją
        echo '<div class="alert alert-danger">' . $_SESSION['error'] . '</div>';
        unset($_SESSION['error']);
    }

    if (isset($_SESSION['success']) && $_SESSION['success']) {// wyświetlanie potwierdzeń
        echo '<div class="alert alert-success">' . $_SESSION['success'] . '</div>';
        unset($_SESSION['success']);
    }
}

function miniatura($src, $dest, $desired_width, $extension) { // generowanie miniatury
    if ($extension == "jpg") { // jeśli jpg
        if ($source_image = imagecreatefromjpeg($src)) {

            $width = imagesx($source_image);
            $height = imagesy($source_image);

            /* find the "desired height" of this thumbnail, relative to the desired width  */
            $desired_height = floor($height * ($desired_width / $width));

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

            /* copy source image at a resized size */
            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

            /* create the physical thumbnail image to its destination */
            imagejpeg($virtual_image, $dest);
        }
    }
    if ($extension == "png") { // jeśli png
        /* read the source image */
        if ($source_image = imagecreatefrompng($src)) {

            $width = imagesx($source_image);
            $height = imagesy($source_image);

            /* find the "desired height" of this thumbnail, relative to the desired width  */
            $desired_height = floor($height * ($desired_width / $width));

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

            /* copy source image at a resized size */
            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

            /* create the physical thumbnail image to its destination */
            imagepng($virtual_image, $dest);
        }
    }
}
